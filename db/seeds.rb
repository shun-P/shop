# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)
Product.create!(
    name: "冷蔵庫",
    price: 80000,
)

Product.create!(
    name: "電子レンジ",
    price: 20000,
)

Product.create!(
    name: "三輪車",
    price: 19800,
)